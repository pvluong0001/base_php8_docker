### 1. Copy .env.example -> .env 'trong thư mục gốc' và cấu hình các tham số cơ bản như database config, web port, ...
```text
# id user hiện tại. Mặc định là 1000. Chắc cú thì chạy câu lệnh "echo $UID"
USER_ID=

# port để có thể truy cập đến db trong container từ máy local
DB_EXPOSE_PORT=
# tên database mặc định
DB_DATABASE=
# tên user name
DB_USERNAME=
# password tương ứng với user ở trên
DB_PASSWORD=
# password của user "root"
DB_ROOT_PASSWORD=

# port của web. Sau khi start truy cập http://localhost:<port>, tương tự với vue + nuxt app
ADMIN_PORT=
VUE_HOST_PORT=
NUXT_HOST_PORT=
REDIS_HOST_PORT=
```

### 2. Builder container
```shell
docker-compose build
```

### 3. Run container:
```shell
// run all services
docker-compose up
```

```shell
// run specific services 
docker-compose up admin vue nginx mysql
```

### 4. Config:
#### 4.1 Database:
```text
DB_CONNECTION=mysql
# trùng tên service mysql trong file docker-compose.yml
DB_HOST=db
DB_PORT=3306
# cấu hình dựa vào file .env đã config ở trên
DB_DATABASE=zinza
DB_USERNAME=zinza
DB_PASSWORD=123@123a
```

### 4. Chạy các câu lệnh init cơ bản(sau khi install composer + init .env file):
```shell
docker-compose exec admin php artisan key:generate
docker-compose exec admin php artisan migrate
```