#!/bin/bash

# Exit on fail
set -e

# Bundle install
yarn install

# Start services
if [ ${ENVIROMENT} = "PROD" ]; then
    yarn run build && yarn build
else
    yarn run serve
fi

# Finally call command issued to the docker service
exec "$@"
