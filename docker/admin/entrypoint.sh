#!/bin/sh
# Exit on fail
set -e

/usr/bin/supervisord
php-fpm

exec "$@"